![image](https://interestedvideos.com/wp-content/uploads/2023/02/golang-gMW2A.jpg)


# 🚀 Go Finance Web

Bem-vindo(a). Este é o Go Finance Web!

O objetivo desta aplicação integrar de forma visual as rotinas do backend go-finance (https://gitlab.com/marceloeduardo244/go-finance), com ferramentas para o controle financeiro que facilitam o seu dia-a-dia.

# 🧠 Contexto

Tecnologias utilizadas neste projeto:
-  React.JS (https://pt-br.reactjs.org/)
-  Next.JS (https://nextjs.org/)
-  Typescript (https://www.typescriptlang.org/)
-  Docker (https://docs.docker.com/get-started/overview/)
-  Kubernetes (https://kubernetes.io/docs/concepts/overview/)
-  Pipeline End to End (https://gitlab.com/marceloeduardo244/go-finance/-/pipelines)
    - Na pipeline temos 4 jobs
        - run_build_test para testar a fase de build da aplicação
        - deploy_batch_docker para fazer o build e atualizar a imagem no docker-hub

## 📋 Instruções para subir a aplicação utilizando Docker-compose

É necessário ter o Docker e Docker-compose instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- docker-compose up -d
- Vc terá 5 novos container rodando na sua maquina.
- O backend rodando por padrão na porta localhost:8080, do Postgres na porta localhost:5432, o pgadmin na porta localhost:80 e o Go Finance Web rodando na porta 3000.
- Agora vc terá tbm o servidor proxy da aplicação, com os seguintes endereços
    - 80 - frontend
    - 9090 - backend
    - 9091 - pgadmin
    - 9092 - postgres
- Para rodar o servidor execute os comandos abaixo na raiz do projeto
    - cd ./kubernetes/proxy-server/config
    - Edite o arquivo nginx.conf modificando o valor IPdaMAQUINA pelo ip da maquina onde cluster kubernetes foi iniciado.
- Para subir o container rode o comando:
        - 	docker run --name go-finance-proxy-kubernetes -v caminho_até_o_arquivo_nginx/nginx.conf:/etc/nginx/nginx.conf -p 80:80 -p 9090:9090 -p 9091:9091 -p 9092:9092 -d marcelosilva404/go-finance-proxy:1.0
- Abaixo nesta doc tem a documentação das telas.

## 📋 Instruções para subir a aplicação utilizando o Kubernetes

É necessário ter o Docker e Docker-compose e Kubernets instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- cd devops
- kubectl apply -f ./kubernetes
- Vc terá 9 novos container rodando na sua maquina.
- O backend rodando por padrão na porta localhost:30002, o Postgres poderá ser acessado via pgadmin na porta localhost:30003 e o Go Finance Web na porta localhost:30004.
- Agora vc terá tbm o servidor proxy da aplicação, com os seguintes endereços
    - 80 - frontend
    - 9090 - backend
    - 9091 - pgadmin
    - 9092 - postgres
- Abaixo nesta doc tem a documentação das telas.

## ✔️ Telas da APP

Alem da breve descrição abaixo sobre as telas.:

# Telas
- /signin | Login no sistema. (https://gitlab.com/marceloeduardo244/go-finance-web/-/blob/main/docs/login_page.jpg)
- /signup | Cria novo usuário no sistema. (https://gitlab.com/marceloeduardo244/go-finance-web/-/blob/main/docs/signup_page.jpg)
- /home | Tela de dashboard com detalhes financeiros. (https://gitlab.com/marceloeduardo244/go-finance-web/-/blob/main/docs/dashboard_page.jpg)
- /DebitCategories | Tela que exibe e adiciona somente as categorias de Debito no sistema.
- /ReceiptCategories | Tela que exibe e adiciona somente as categorias de Receita no sistema.
- /Receipt | Tela que exibe todas as receitas por periodo de tempo e permite adicionar nova.
- /Debit | Tela que exibe todos os débitos por periodo de tempo e permite adicionar novo.

## Video utilizando o sistema
- https://gitlab.com/marceloeduardo244/go-finance-web/-/blob/main/docs/utilizando-o-sistema.mp4

# Backend da Aplicação
- https://gitlab.com/marceloeduardo244/go-finance

# Integração completa da aplicação
- https://gitlab.com/marceloeduardo244/go-finance-integrado

Made with 💜 at OliveiraDev

