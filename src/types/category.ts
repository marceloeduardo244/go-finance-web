export type CategoryDetails = {
    user_id?: any;
    id?: number;
    title: string;
    type: string;
    description: string;
  };

  export type CategoryContextData = {
    createCategory(categoryDetails: CategoryDetails): any;
    deleteCategory(id: number): any;
    updateCategory(user_id: number, categoryDetails: CategoryDetails): any; 
    getCategoryById(id: number): any;
    getAllCategoriesByUserId(user_id: number, type: string): any;
  };