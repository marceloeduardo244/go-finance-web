export type AccountDetails = {
  id?: number,
  user_id?: any,
  category_id?: any,
  title: string,
  type?: string,
  description: string,
  value: any,
  date?: Date,
  created_at?: Date,
  category_title?: any
  };

  export type AccountContextData = {
    createAccount(AccountDetails: AccountDetails): any;
    deleteAccount(id: number): any;
    updateAccount(user_id: number, AccountDetails: AccountDetails, type: string): any; 
    getAccountById(id: number): any;
    getAllAccountsByUserId(user_id: number, type: string): any;
    getAccountGraph(user_id: number, type: string): any;
    getAccountReport(user_id: number, type: string): any;
  };