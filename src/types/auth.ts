export type LoginCredentials = {
  userName: string;
  password: string;
};

export type LoggedUserDetails = {
  user: string;
  id: string;
  email: string;
};

export type AuthContextData = {
  login(credentials: LoginCredentials): Promise<void>;
  logout(): void;
  loggedUserDetails(): any; 
};
