import { useRouter } from "next/router";
import {
  ReactNode,
  useCallback,
  useEffect,
  useState
} from "react";
import AuthContext from "../context/AuthContext";
import api from "../services/api";
import { LoginCredentials } from "../types/auth";

type AuthProviderProps = {
  children: ReactNode;
};

type AuthState = {
  token: string;
};

const AuthProvider = ({ children }: AuthProviderProps) => {
  const router = useRouter();
  const [origin, setOrigin] = useState("/");
  const [data, setData] = useState<AuthState | undefined>(() => {
    if (typeof window === "undefined") return {} as AuthState;
    const token = localStorage.getItem("@gofinance:token");
    if (token) {
      api.defaults.headers.authorization = `Bearer ${token}`;
      return { token };
    }
  });
  useEffect(() => {
    if (
      !data?.token &&
      router.pathname !== "/signin" &&
      router.pathname !== "/signup"
    ) {
        setOrigin(router.pathname);
        router.replace("/signin");
    }
  }, [data, router]);
  const getUserData = useCallback(
    async (userName: String) => {
      const response = await api.get(`/user/${userName}`);
      const idAndEmailofUser = response.data;
      localStorage.setItem("@gofinance:id", idAndEmailofUser.id);
      localStorage.setItem("@gofinance:email", idAndEmailofUser.email);
      router.replace(origin);
    },
    [origin, router]
  );
  const login = useCallback(
    async ({ userName, password }: LoginCredentials) => {
      const response = await api.post("/login", {
        userName,
        password,
      });
      const accessToken = response.data;
      localStorage.setItem("@gofinance:token", accessToken);
      localStorage.setItem("@gofinance:user", userName);
      api.defaults.headers.authorization = `Bearer ${accessToken}`;
      router.replace(origin);
      if (!accessToken) {
        setData(undefined);
      }
      setData({ token: accessToken });
      console.log("userName")
      console.log(userName)
      getUserData(userName)
    },
    [origin, router]
  );
  const logout = useCallback(() => {
    localStorage.removeItem("@gofinance:token");
    localStorage.removeItem("@gofinance:user");
    localStorage.removeItem("@gofinance:id");
    localStorage.removeItem("@gofinance:email");
    delete api.defaults.headers.authorization;
    setData(undefined);
  }, []);

  const loggedUserDetails = () => {
    let user : string | null = "";
    let id : string | null = "0";
    let email : string | null = "";

    if (typeof window !== 'undefined') {
      // Perform localStorage action
      user = localStorage.getItem("@gofinance:user");
      id = localStorage.getItem("@gofinance:id");
      email = localStorage.getItem("@gofinance:email");
    }

    return {
      user,
      id,
      email
    }
  };
  return (
    <AuthContext.Provider value={{ login, logout, loggedUserDetails }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider ;
