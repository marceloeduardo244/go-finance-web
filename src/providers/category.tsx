import {
  ReactNode} from "react";
import CategoryContext from "../context/CategoryContext";
import api from "../services/api";
import { CategoryDetails } from "@/types/category";

type CategoryProviderProps = {
  children: ReactNode;
};

const CategoryProvider = ({ children }: CategoryProviderProps) => {
  const createCategory = async (categoryDetails: CategoryDetails) => {
    categoryDetails.user_id = parseInt(categoryDetails.user_id)
    const response = await api.post("/category", categoryDetails);
    const newCategory = response.data;

    return newCategory
  };

  const deleteCategory = async (id: number) => (
    await api.delete(`/category/${id}`)
  );

  const updateCategory = (user_id: number, categoryDetails: CategoryDetails) => (
    console.log("createCategory")
  );
  
  const getCategoryById = (id: number) => (
    console.log("createCategory")
  );

  const getAllCategoriesByUserId = async (user_id: number, type: string) => {
      const response = await api.get(`/category/all/${user_id}/${type}`);
      const categories = response.data;

      return categories
  };

  return (
    <CategoryContext.Provider value={{ 
        createCategory, 
        deleteCategory, 
        updateCategory,
        getCategoryById,
        getAllCategoriesByUserId
         }}>
      {children}
    </CategoryContext.Provider>
  );
};

export default CategoryProvider ;
