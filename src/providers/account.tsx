import {
  ReactNode
} from "react";
import AccountContext from "../context/AccountContext";
import api from "../services/api";
import { AccountDetails } from "@/types/account";

type AccountProviderProps = {
  children: ReactNode;
};

const AccountProvider = ({ children }: AccountProviderProps) => {
  const createAccount = async (accountDetails: AccountDetails) => {
    accountDetails.user_id = parseInt(accountDetails.user_id)
    accountDetails.category_id = parseInt(accountDetails.category_id)
    const response = await api.post("/account", accountDetails);
    const newCategory = response.data;

    return newCategory
  };

  const deleteAccount = async (id: number) => (
    await api.delete(`/account/${id}`)
  );

  const updateAccount = async (user_id: number, accountDetails: AccountDetails, type: string) => {
    try {
      accountDetails.value = parseInt(accountDetails.value)
      await api.put(`/account/${user_id}`, accountDetails);
    } catch (error) {
      window.alert(`Erro ao salvar alteração: ${error}`)
    }
  };

  const getAccountById = (id: number) => (
    console.log("createAccount")
  );

  const getAllAccountsByUserId = async (user_id: number, type: string) => {
    const response = await api.get(`/account/all/${user_id}/${type}`);
    const categories = response.data;

    return categories
  };

  const getAccountGraph = async (user_id: number, type: string) => {
    const response = await api.get(`/account/graph/${user_id}/${type}`);
    const categories = response.data;

    return categories
  };

  const getAccountReport = async (user_id: number, type: string) => {
    const response = await api.get(`/account/reports/${user_id}/${type}`);
    const categories = response.data;

    return categories
  };

  return (
    <AccountContext.Provider value={{
      createAccount,
      deleteAccount,
      updateAccount,
      getAccountById,
      getAllAccountsByUserId,
      getAccountGraph,
      getAccountReport
    }}>
      {children}
    </AccountContext.Provider>
  );
};

export default AccountProvider;
