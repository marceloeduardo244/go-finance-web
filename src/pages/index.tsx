import Chart from "../components/Chart";
import Card from "../components/Card";

import { Container, RightSide } from "../styles/home/home.styles";
import useLogin from "@/hooks/useLogin";
import useAccounts from "@/hooks/useAccounts";
import { useEffect, useState } from "react";

const Home = () => {
  const [reportReceipt, setReportReceipt] = useState(0)
  const [reportDebit, setReportDebit] = useState(0)
  const [graphReceipt, setGraphReceipt] = useState(0)
  const [graphDebit, setGraphDebit] = useState(0)

  const { loggedUserDetails } = useLogin()
  const { getAccountGraph, getAccountReport  } = useAccounts();

  const { id } = loggedUserDetails()

  const handleGetReportOfReceipts = async (id: number) => {
    const receipts = await getAccountReport(id, 'receipt')

    setReportReceipt(receipts)
  }

  const handleGetReportOfDebits = async (id: number) => {
    const debits = await getAccountReport(id, 'debit')

    setReportDebit(debits)
  }

  const handleGetGraphOfReceipts = async (id: number) => {
    const receipts = await getAccountGraph(id, 'receipt')

    setGraphReceipt(receipts)
  }

  const handleGetGraphOfDebits = async (id: number) => {
    const debits = await getAccountGraph(id, 'debit')

    setGraphDebit(debits)
  }

  useEffect(() => {
    if (id) {
      handleGetReportOfReceipts(id)
      handleGetReportOfDebits(id)
      handleGetGraphOfReceipts(id)
      handleGetGraphOfDebits(id)
    }
  }, [id])

  const cardData = [
    {
      title: "Saldo a pagar",
      value: `R$ ${reportDebit}`,
    },
    {
      title: "Saldo a receber",
      value: `R$ ${reportReceipt}`,
    },
    {
      title: "Saldo total",
      value: `R$ ${reportReceipt - reportDebit}`,
    },
  ];


  return (<Container>
    <Chart debits={reportDebit} receipts={reportReceipt} />
    <RightSide>
      {cardData.map((card) => (
        <Card key={card.title} title={card.title} value={card.value} />
      ))}
    </RightSide>
  </Container>
  )
};

export default Home;
