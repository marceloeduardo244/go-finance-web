import { Container, Content } from "@/styles/pages/DebitCategories/debitCategories.styles";
import TableContainerAccount from "@/components/TableContainerAccount";
import TableDetailsAccount from "@/components/TableDetailsAccount";
import TableLineAccount from "@/components/TableLineAccount";
import useLogin from "@/hooks/useLogin";
import { useEffect, useState } from "react";
import { AccountDetails } from "@/types/account";
import useAccounts from "@/hooks/useAccounts";

const Debit = () => {
  const [updated, setUpdated] = useState<boolean>(false)
  const { loggedUserDetails } = useLogin()
  const { getAllAccountsByUserId, deleteAccount } = useAccounts();

  const { id } = loggedUserDetails()

  const [localDebitAccounts, setLocalDebitAccounts] = useState<AccountDetails[]>([])

  const handleGetAccounts = async (id: number) => {
    const accounts = await getAllAccountsByUserId(id, "debit")
    setLocalDebitAccounts(accounts)
  } 

  const handleDeleteCategory = async (id: number) => {
    await deleteAccount(id)
  }

  useEffect(() => {
    if (id) {
      handleGetAccounts(id)
      setUpdated(false)
    }
  }, [id, updated])

    return (
      <Container>
      <Content>
        <h3>Movimentações | Débitos</h3>
        <hr />
        <TableContainerAccount type="debit" user_id={id} setUpdated={setUpdated}>
          <TableDetailsAccount>
            {localDebitAccounts.length > 0 ? localDebitAccounts.map((cat) => (
              <TableLineAccount
                key={cat.id}
                id={cat.id!}
                title={cat.title}
                description={cat.description}
                category_title={cat.category_title}
                date={cat.date}
                value={cat.value}
                deleteCategory={handleDeleteCategory}
                setUpdated={setUpdated}
                type={cat.type}
              />
            )) : <p>Nenhuma conta encontrada</p>}
          </TableDetailsAccount>
        </TableContainerAccount>
      </Content>
    </Container>
    );
};

export default Debit;
