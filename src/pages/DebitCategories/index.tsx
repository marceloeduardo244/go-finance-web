import { Container, Content } from "@/styles/pages/DebitCategories/debitCategories.styles";
import TableContainer from "@/components/TableContainer";
import TableDetails from "@/components/TableDetails";
import TableLine from "@/components/TableLine";
import useCategory from "@/hooks/useCategories";
import useLogin from "@/hooks/useLogin";
import { useEffect, useState } from "react";
import { CategoryDetails } from "@/types/category";

const DebitCategories = () => {
  const [updated, setUpdated] = useState<boolean>(false)
  const { loggedUserDetails } = useLogin()
  const { getAllCategoriesByUserId, deleteCategory } = useCategory();

  const { id } = loggedUserDetails()

  const [localDebitCategories, setLocalDebitCategories] = useState<CategoryDetails[]>([])

  const handleGetCategories = async (id: number) => {
    const categories = await getAllCategoriesByUserId(id, "debit")
    setLocalDebitCategories(categories)
  } 

  const handleDeleteCategory = async (id: number) => {
    await deleteCategory(id)
  }

  useEffect(() => {
    if (id) {
      handleGetCategories(id)
      setUpdated(false)
    }
  }, [id, updated])

  return (
    <Container>
      <Content>
        <h3>Categorias | Débito</h3>
        <hr />
        <TableContainer type="debit" user_id={id} setUpdated={setUpdated}>
          <TableDetails>
            {localDebitCategories.length > 0 ? localDebitCategories.map((cat) => (
              <TableLine
                key={cat.id}
                id={cat.id!}
                title={cat.title}
                description={cat.description}
                deleteCategory={handleDeleteCategory}
                setUpdated={setUpdated}
              />
            )) : <p>Nenhuma categoria encontrada</p>}
          </TableDetails>
        </TableContainer>
      </Content>
    </Container>
  );
};

export default DebitCategories;
