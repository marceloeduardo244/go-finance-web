import { Container, Content } from "@/styles/pages/ReceiptCategories/receiptCategories.styles";
import TableContainer from "@/components/TableContainer";
import TableDetails from "@/components/TableDetails";
import TableLine from "@/components/TableLine";
import useCategory from "@/hooks/useCategories";
import useLogin from "@/hooks/useLogin";
import { CategoryDetails } from "@/types/category";
import { useState, useEffect } from "react";

const ReceiptCategories = () => {
  const [updated, setUpdated] = useState<boolean>(false)
  const { loggedUserDetails } = useLogin()
  const { getAllCategoriesByUserId, deleteCategory } = useCategory();

  const { id } = loggedUserDetails()

  const [localReceiptCategories, setLocalReceiptCategories] = useState<CategoryDetails[]>([])

  const handleGetCategories = async (id: number) => {
    const categories = await getAllCategoriesByUserId(id, "receipt")
    setLocalReceiptCategories(categories)
  } 

  const handleDeleteCategory = async (id: number) => {
    await deleteCategory(id)
  }

  useEffect(() => {
    if (id) {
      handleGetCategories(id)
      setUpdated(false)
    }
  }, [id, updated])

  return (
    <Container>
      <Content>
        <div><h3>Categorias | Receitas</h3></div>
        <hr />
        <TableContainer type="receipt" user_id={id} setUpdated={setUpdated}>
          <TableDetails>
            {localReceiptCategories.length > 0 ? localReceiptCategories.map((cat) => (
              <TableLine
                key={cat.id}
                id={cat.id!}
                title={cat.title}
                description={cat.description}
                deleteCategory={handleDeleteCategory}
                setUpdated={setUpdated}
              />
            )) : <p>Nenhuma categoria encontrada</p>}
          </TableDetails>
        </TableContainer>
      </Content>
    </Container>
  );
};

export default ReceiptCategories;
