import {  useContext } from "react";
import CategoryContext from "../context/CategoryContext";
import { CategoryContextData } from "../types/category";


const useCategory = (): CategoryContextData => {
  const context = useContext(CategoryContext);
  return context;
};

export default useCategory;
