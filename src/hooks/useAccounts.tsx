import { useContext } from "react";
import AccountContext from "../context/AccountContext";
import { AccountContextData } from "../types/account";


const useAccounts = (): AccountContextData => {
  const context = useContext(AccountContext);
  return context;
};

export default useAccounts;
