import { ReactNode } from "react";
import { Body, Container, Header } from "./styles";

interface TableDetailsAccountProps {
    children: ReactNode;
}

const TableDetailsAccount = ({children}: TableDetailsAccountProps) => {
  return (
    <Container>
      <Header>
        <h1>Título</h1>
        <h1 id="optional">Descrição</h1>
        <h1 id="optional">Data</h1>
        <h1 id="optional">Categoria</h1>
        <h1>Valor</h1>
      </Header>
      <Body>
        {children}
      </Body>
    </Container>
  );
};

export default TableDetailsAccount;
