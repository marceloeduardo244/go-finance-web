import styled from "styled-components";

export const Container = styled.div`
  background-color: ${(props) => props.theme.colors.black1};
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  margin-top: 8px;
`;

export const Button = styled.button`
  background-color: ${(props) => props.theme.colors.primary};
  width: 60%;
  height: 25px;
  font-size: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Labels = styled.h3`
  width: 100%;
  height: 60px;
  font-size: 0.6rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 4px;
`;

export const Label = styled.h3`
  width: 100%;
  height: 30px;
  padding: 4px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
`;