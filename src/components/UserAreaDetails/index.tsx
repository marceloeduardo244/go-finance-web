import { Container, Button, Labels, Label } from "./styles";
import useLogin from "../../hooks/useLogin"
import Image from "next/image";
import Link from "next/link";

import logo from "../../assets/logo.png";

const UserAreaDetail = () => {
  const { logout, loggedUserDetails } = useLogin();

  const userDetail = loggedUserDetails();
  return (
    <Container>
      <Link href="/">
        <Image src={logo} alt="GoFinance" width={60} />
      </Link>
      <Labels>
        <Label>Bem vindo(a) {userDetail.user}</Label>
      </Labels>
      <Button onClick={logout}>Sair</Button>
    </Container>
  );
};

export default UserAreaDetail;
