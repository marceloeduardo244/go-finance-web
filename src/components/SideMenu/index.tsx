import Image from "next/image";
import Link from "next/link";

import logo from "../../assets/logo.png";
import debit from "../../assets/debit.png";
import receipt from "../../assets/receipt.png";
import debitCategories from "../../assets/debitCategories.png";
import receiptCategories from "../../assets/receiptCategories.png";

import { Container, MenuItems, Option, Label } from "./styles";
import UserAreaDetail from "../UserAreaDetails";

const menuItems = [
  {
    href: "/",
    src: logo,
    alt: 'home',
    label: 'Dashboard'
  },
  {
    href: "/Debit",
    src: debit,
    alt: 'debit',
    label: 'Débitos'
  },
  {
    href: "/Receipt",
    src: receipt,
    alt: 'receipt',
    label: 'Receitas'
  },
  {
    href: "/DebitCategories",
    src: debitCategories,
    alt: 'debitCategories',
    label: 'Cetegorias | Débito'
  },
  {
    href: "/ReceiptCategories",
    src: receiptCategories,
    alt: 'receiptCategories',
    label: 'Categorias | Receitas'
  }
];

const SideMenu = () => {
  return (
    <Container>
      <UserAreaDetail />
      <MenuItems>
        {menuItems.map((menuitem) => (
          <Option key={menuitem.href}>
            <Link key={menuitem.href} href={menuitem.href} >
              <Image src={menuitem.src} alt="/" width={41} />
            </Link>
            <Link key={menuitem.href} href={menuitem.href} >
              <Label>{menuitem.label}</Label>
            </Link>
          </Option>
        ))}
      </MenuItems>
    </Container>
  );
};

export default SideMenu;
