import styled from "styled-components";

export const Container = styled.div`
  background-color: ${(props) => props.theme.colors.black1};
  height: 100%;
  width: 16%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;

export const MenuItems = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  gap: 20px;
  align-items: center;
`;

export const Option = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  align-items: center;
  gap: 8px;
`;

export const Label = styled.h6`
    color: white;
    text-decoration: none;
    @media (max-width: 1000px) {
      display: none;
    }
`;
