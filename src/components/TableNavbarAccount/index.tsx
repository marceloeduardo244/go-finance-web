import { useEffect, useState } from "react";
import { Container, LeftSide } from "./styles";
import useAccount from "@/hooks/useAccounts";
import PersonalizatedAccountModal from "../AccountModal";

const TableNavbarAccount = ({ type, user_id, setUpdated }: { type: string, user_id: number, setUpdated: any }) => {
  const { createAccount } = useAccount()

  const [title, setTitle] = useState<string>("")
  const [description, setDescription] = useState<string>("")
  const [value, setValue] = useState<string>("")
  const [categoryId, setCategoryId] = useState<any>(0)

  const [open, setOpen] = useState<boolean>(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = async () => {
    var time = new Date().getTime(); // get your number
    var date = new Date(time); // create Date object

    if (categoryId != 0) {
      console.log({
        user_id: user_id,
        category_id: categoryId,
        type,
        description,
        title,
        value: parseInt(value),
        date
      })
  
      await createAccount({
        user_id: user_id,
        category_id: categoryId,
        type,
        description,
        title,
        value: parseInt(value),
        date
      })
  
      setUpdated(true)
    } else {
      window.alert("Selecione a categoria")
    }
  }

  return (
    <Container>
      <LeftSide>
        {open ? <PersonalizatedAccountModal
          open={open}
          handleClose={handleClose}
          complement={type == "receipt" ? "Receita" : "Debito"}
          setTitle={setTitle}
          setDescription={setDescription}
          setCategoryId={setCategoryId}
          setValue={setValue}
          handleSave={handleSave}
        /> : null}
      </LeftSide>
      <button onClick={handleOpen}>Adicionar</button>
    </Container>
  )
};

export default TableNavbarAccount;
