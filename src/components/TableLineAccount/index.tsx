import Image from "next/image";
import { Container } from "./styles";

import deleteIcon from '../../assets/delete.png'
import editIcon from '../../assets/pencil.png'
import { useState } from "react";
import PersonalizatedAccountModal from "../AccountModal";
import useAccounts from "@/hooks/useAccounts";
import useLogin from "@/hooks/useLogin";

interface TableLineAccountProps {
  id: number;
  title: string;
  description: string;
  deleteCategory: any;
  setUpdated: any;
  date: any;
  category_title: { String: any };
  value: any;
  type?: string;
}

const TableLineAccount = (
  {
    id,
    title,
    description,
    deleteCategory,
    setUpdated,
    date,
    category_title,
    value,
    type }: TableLineAccountProps) => {

  const { updateAccount } = useAccounts()
  const { loggedUserDetails } = useLogin()

  const loggedUser = loggedUserDetails()

  const [titleEdit, setTitle] = useState<string>("")
  const [descriptionEdit, setDescription] = useState<string>("")
  const [valueEdit, setValue] = useState<string>("")

  const [open, setOpen] = useState<boolean>(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDeleteCategory = async () => {
    await deleteCategory(id)
    setUpdated(true)
  }

  const handleEditCategory = async () => {
    const dto = {
      id,
      title: titleEdit == '' ? title : titleEdit,
      description: descriptionEdit == '' ? description : descriptionEdit,
      value: valueEdit == '' ? value : valueEdit
    }

    if (type) {
      updateAccount(loggedUser.id, dto, type)
      setUpdated(true)
    }
  }

  const convertedDate = new Date(date);

  const year = convertedDate.getFullYear();
  const month = ("0" + (convertedDate.getMonth() + 1)).slice(-2); // add leading zero for single digit month
  const day = ("0" + convertedDate.getDate()).slice(-2); // add leading zero for single digit day
  const formattedDate = `${day}/${month}/${year}`;

  return (
    <Container>
      {open ? <PersonalizatedAccountModal
          open={open}
          handleClose={handleClose}
          complement={type == "receipt" ? "Receita" : "Debito"}
          setTitle={setTitle}
          setDescription={setDescription}
          setValue={setValue}
          handleSave={handleEditCategory}
          isEdit={true}
          actualTitle={title}
          actualDescription={description}
          actualValue={value}
          handleEditCategory={handleEditCategory}
        /> : null}
      <span>{title}</span>
      <span id="optional">{description}</span>
      <span id="optional">{formattedDate}</span>
      <span id="optional">{category_title.String}</span>
      <span>{value}</span>
      <Image src={editIcon} onClick={handleOpen} alt="" width={40} />
      <Image src={deleteIcon} onClick={handleDeleteCategory} alt="" width={24} />
    </Container>
  );
};

export default TableLineAccount;
