import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  margin-bottom: 20px;

  span{
    font-size: 16px;
    width: 20%;

    @media (max-width: 600px) {
      width: 50%;
    }
  }
  
  span#optional {
    @media (max-width: 600px) {
    display: none;
    }
  }

  img {
    width: 2rem;
    cursor: pointer;
    padding-right: 8px;

    @media (max-width: 600px) {
    display: none;
    }
  }
`;
