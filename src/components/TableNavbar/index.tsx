import { useEffect, useState } from "react";
import DateInput from "../DateInput";
import SearchBar from "../SearchBar";
import { Container, LeftSide } from "./styles";
import PersonalizatedModal from "../CategoryModal";
import useCategory from "@/hooks/useCategories";

const TableNavbar = ({ type, user_id, setUpdated }: { type: string, user_id: number, setUpdated: any}) => {
  const { createCategory } = useCategory()

  const [title, setTitle] = useState<string>("")
  const [description, setDescription] = useState<string>("")

  const [open, setOpen] = useState<boolean>(false);

  const handleOpen = () => {
    console.log("open")
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = async () => {
    console.log(user_id)

    await createCategory({
      user_id: user_id,
      type,
      description,
      title
    })

    setUpdated(true)
  }

  return (
    <Container>
      <LeftSide>
        {open ? <PersonalizatedModal 
        open={open} 
        handleClose={handleClose} 
        complement={type == "receipt" ? "Receita" : "Debito"}
        setTitle={setTitle}
        setDescription={setDescription}
        handleSave={handleSave}
        /> : null}
      </LeftSide>
      <button onClick={handleOpen}>Adicionar</button>
    </Container>
  )
};

export default TableNavbar;
