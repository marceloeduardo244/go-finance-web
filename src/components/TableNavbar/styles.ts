import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    width: 100%;

    button{
        padding: 0px;
        width: 9rem;
        height: 50px;

        @media (max-width: 600px) {
            display: none;
        }
    }
`
export const LeftSide = styled.div`
    display: flex;
    gap: 20px;
    align-items: flex-end;
`
