import { ReactNode } from "react";
import TableNavbar from "../TableNavbar";
import { Container } from "./styles";

interface TableContainerProps {
  children: ReactNode;
  type: string;
  user_id: number;
  setUpdated: any;
}

const TableContainer = ({ children, type, user_id, setUpdated }: TableContainerProps) => {
  return (
    <Container>
      <TableNavbar type={type} user_id={user_id} setUpdated={setUpdated} />
      {children}
    </Container>
  );
};

export default TableContainer;
