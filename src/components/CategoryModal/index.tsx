import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import { Container, ContainerContent } from './styles';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '80%',
    bgcolor: '#171616',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
    borderRadius: '8px',
    borderColor: '#FF7A00'
};

const PersonalizatedModal = ({ 
    open, 
    handleClose, 
    complement,
    setTitle,
    setDescription,
    handleSave }: 
    { 
        open: boolean, 
        handleClose: any, 
        complement: string
        setTitle: any
        setDescription: any,
        handleSave: any }) => {

    return (
        <Container>
            <React.Fragment>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="child-modal-title"
                    aria-describedby="child-modal-description"
                >
                    <Box sx={{ ...style }}>
                        <ContainerContent>
                            <p><h2 id="child-modal-title">Cadastrar categoria | {complement}</h2></p>
                            <div>
                                <input type='text' placeholder='Titulo' onChange={(e: any) => setTitle(e.target.value)} />
                                <input type='text' placeholder='Descrição' onChange={(e: any) => setDescription(e.target.value)} />
                            </div>
                            <div>
                                <button onClick={handleSave}>Cadastar</button>
                                <button id="cancelar" onClick={handleClose}>Cancelar</button>
                            </div>
                        </ContainerContent>
                    </Box>
                </Modal>
            </React.Fragment>
        </Container>
    )
};

export default PersonalizatedModal;