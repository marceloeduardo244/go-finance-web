import { ReactNode } from "react";
import TableNavbar from "../TableNavbar";
import { Container } from "./styles";
import TableNavbarAccount from "../TableNavbarAccount";

interface TableContainerAccountProps {
  children: ReactNode;
  type: string;
  user_id: number;
  setUpdated: any;
}

const TableContainerAccount = ({ children, type, user_id, setUpdated }: TableContainerAccountProps) => {
  return (
    <Container>
      <TableNavbarAccount type={type} user_id={user_id} setUpdated={setUpdated} />
      {children}
    </Container>
  );
};

export default TableContainerAccount;
