import { ReactElement, useState } from 'react';
import { Container } from './styles';

const DateInput = ({title, setDate}: { title: string, setDate: any}) => {
    return (
        <Container>
            <h1>{title}</h1>
            <input type="date" onChange={(e: any) => setDate(e.target.value)}/>
        </Container>
    )
}

export default DateInput;