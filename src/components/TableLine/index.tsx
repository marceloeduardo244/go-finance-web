import Image from "next/image";
import { Container } from "./styles";

import deleteIcon from '../../assets/delete.png'

interface TableLineProps {
    id: number;
    title: string;
    description: string;
    deleteCategory: any;
    setUpdated: any;
}

const TableLine = ({id, title, description, deleteCategory, setUpdated}:TableLineProps) => {
  const handleDeleteCategory = async () => {
    await deleteCategory(id)
    setUpdated(true)
  }

  return (
    <Container>
      <span>{title}</span>
      <span>{description}</span>
      <Image src={deleteIcon} onClick={handleDeleteCategory} alt="" width={24} />
    </Container>
  );
};

export default TableLine;
