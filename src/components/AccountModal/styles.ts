import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: '100%'
  align-items: center;
  background-color: ${(props) => props.theme.colors.black3};
  border-radius: 4px;
  padding-right: 16px;

  img{
    cursor: pointer;
  }
`;

export const ContainerContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  padding: 4px;

  p {
    margin-bottom: 16px;
  }

  input {
    margin: 4px;
    background-color: #ffffff;
    width: 49%;
    font-size: 1em;
    color: #000000;
  }

  button {
    margin-top: 8px;
  }

  button#cancelar {
    background: red;
  }

  div {
    width: 100%;
    display: flex;
    justify-content: center;
  }
`;
