import * as React from 'react';
import { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { Container, ContainerContent } from './styles';
import useCategory from '@/hooks/useCategories';
import useLogin from '@/hooks/useLogin';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '80%',
    bgcolor: '#171616',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
    borderRadius: '8px',
    borderColor: '#FF7A00'
};

const PersonalizatedAccountModal = ({
    open,
    handleClose,
    complement,
    setTitle,
    setDescription,
    setValue,
    setCategoryId,
    handleSave,
    isEdit,
    actualTitle,
    actualDescription,
    actualValue,
    handleEditCategory }:
    {
        open: boolean,
        handleClose: any,
        complement: string
        setTitle: any
        setDescription: any,
        setCategoryId?: any,
        setValue: any,
        handleSave: any,
        isEdit?: boolean,
        actualTitle?: string;
        actualDescription?: string;
        actualValue?: any;
        handleEditCategory?: any;
    }) => {

    const [categories, setCategories] = useState<any>([])
    const { getAllCategoriesByUserId } = useCategory();
    const { loggedUserDetails } = useLogin()
    const { id } = loggedUserDetails()

    const handleGetCategories = async (id: number) => {
        const type = complement == "Debito" ? "debit" : "receipt"
        const categories = await getAllCategoriesByUserId(id, type)
        setCategories(categories)
    }

    useEffect(() => {
        handleGetCategories(id)
    }, [open])

    const complementName = complement == "Debito" ? "Conta" : "Lucro"

    return (
        <Container>
            <React.Fragment>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="child-modal-title"
                    aria-describedby="child-modal-description"
                >
                    <Box sx={{ ...style }}>
                        <ContainerContent>
                            <p>
                                <h2 id="child-modal-title">Cadastrar {complementName} | {complement}</h2>
                            </p>
                            <div>
                                <input type='text' placeholder='Titulo' defaultValue={isEdit ? actualTitle : ''} onChange={(e: any) => setTitle(e.target.value)} />
                                <input type='text' placeholder='Descrição' defaultValue={isEdit ? actualDescription : ''} onChange={(e: any) => setDescription(e.target.value)} />
                            </div>
                            <div>
                                <input type='text' placeholder='Valor' defaultValue={isEdit ? actualValue : ''} onChange={(e: any) => setValue(e.target.value)} />
                                {isEdit ? null :                                 
                                <select name="categories" onChange={(e: any) => setCategoryId(e.target.value)}>
                                    {categories.length > 0 ? categories.map((cat: any) => (
                                        <option key={cat.id} value={cat.id}>{cat.title}</option>
                                    )) : null}
                                    <option key={10000} selected value={10000}>Selecione</option>
                                </select>}

                            </div>
                            <div>
                                <button onClick={isEdit ? handleEditCategory : handleSave}>{isEdit ? 'Salvar' : 'Cadastrar'}</button>
                                <button id="cancelar" onClick={handleClose}>Cancelar</button>
                            </div>
                        </ContainerContent>
                    </Box>
                </Modal>
            </React.Fragment>
        </Container >
    )
};

export default PersonalizatedAccountModal;