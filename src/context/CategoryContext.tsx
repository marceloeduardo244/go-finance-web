import { createContext } from "react";
import { CategoryContextData,  } from "../types/category";

const CategoryContext = createContext<CategoryContextData>({} as CategoryContextData);

export default CategoryContext;
