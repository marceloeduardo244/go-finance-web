import { createContext } from "react";
import { AccountContextData } from "../types/account";

const CategoryContext = createContext<AccountContextData>({} as AccountContextData);

export default CategoryContext;
