FROM marcelosilva404/go-static-server:1.0

COPY out/ /go/src/gitlab.com/marceloeduardo244/go-static-server/static/

EXPOSE 3000

# Run the executable
CMD ["go-static-server"]
